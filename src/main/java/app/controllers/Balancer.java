package app.controllers;

import app.hosts.Host;
import app.hosts.impl.HostOne;
import app.hosts.impl.HostTwo;
import app.models.Bet;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


@RestController
public class Balancer {
	private Host host;
	private List<Bet> bets = new ArrayList<>();
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping(value = "/bet", method = RequestMethod.GET)
	public Bet bet(@RequestParam(value = "amount", defaultValue = "100") String amount) {
		long id = counter.incrementAndGet();

		if (id % 2 != 0) {
			host = new HostOne();
		} else {
			host = new HostTwo();
		}

		Bet newBet = new Bet(id, Integer.valueOf(amount));
		host.process(getBets(), newBet);

		return newBet;
	}

	@RequestMapping("/bets")
	public List<Bet> bets() {
		return getBets();
	}

	@RequestMapping("/stat")
	public String stat() {
		return "Total bets count: " + getBets().size();
	}

	private List<Bet> getBets() {
		if (bets == null) {
			return Collections.emptyList();
		}
		return bets;
	}

}
