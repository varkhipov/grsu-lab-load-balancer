package app.hosts.impl;

import app.hosts.Host;
import app.models.Bet;

import java.util.List;


public class HostTwo implements Host {

	@Override
	public void process(List<Bet> bets, Bet newBet) {
		newBet.setProcessedByHost(PREFIX + "host 2.");
		bets.add(newBet);
	}
}
