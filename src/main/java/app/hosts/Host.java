package app.hosts;

import app.models.Bet;

import java.util.List;


public interface Host {
	String PREFIX = "Processed by ";

	void process(List<Bet> bets, Bet newBet);
}
