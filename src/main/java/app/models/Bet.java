package app.models;

import java.util.UUID;


public class Bet {
	private final long id;
	private final UUID uid;
	private int amount;
	private String processedByHost;

	public Bet(long id, int amount) {
		this.id = id;
		this.uid = UUID.randomUUID();
		this.amount = amount;
	}

	public long getId() {
		return id;
	}

	public UUID getUid() {
		return uid;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getProcessedByHost() {
		return processedByHost;
	}

	public void setProcessedByHost(String processedByHost) {
		this.processedByHost = processedByHost;
	}

	@Override
	public String toString() {
		return "Bet{" +
				"id=" + id +
				", uid=" + uid +
				", amount=" + amount +
				", processedByHost='" + processedByHost + '\'' +
				'}';
	}
}
